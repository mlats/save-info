package com.example.homework6

import android.app.Activity
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE)
        read()
        savebtn.setOnClickListener {
            if ((Email.text.toString().trim().isNotEmpty()) && (FirstName.text.toString().trim().isNotEmpty())
                    && (LastName.text.toString().trim().isNotEmpty()) && (Age.text.toString().trim().isNotEmpty())
                    && (Address.text.toString().trim().isNotEmpty())) {
                save()
            } else
            {   Toast.makeText(applicationContext, "Some Fields Are Empty", Toast.LENGTH_SHORT).show() }
        }
    }


    fun save() {
        val email = Email.text.toString()
        val firstname = FirstName.text.toString()
        val lastname = LastName.text.toString()
        val age = Age.text.toString()
        val address = Address.text.toString()

        val editor = sharedPreferences.edit()
        editor.putString("email", email)
        editor.putString("firstname", firstname)
        editor.putString("lastname", lastname)
        editor.putString("age", age)
        editor.putString("address", address)
        editor.apply()
    }

    fun read()
    {
        val email = sharedPreferences.getString("email", "")
        val firstname = sharedPreferences.getString("firstname", "")
        val lastname = sharedPreferences.getString("lastname", "")
        val age = sharedPreferences.getString("age", "0")
        val address = sharedPreferences.getString("address", "")

        Email.setText(email)
        FirstName.setText(firstname)
        LastName.setText(lastname)
        Age.setText(age)
        Address.setText(address)
    }
}